#include <iostream>
#include <string>
#include "pledge.h"

using namespace std;

int bruteforce(string word, string pattern);
string copy(string s, int a, int b);

int main(){
	pledge();

	string text;
	string pattern;
	int start = 0;

	cout << "Enter a string of text 20 characters long" << endl;
	getline(cin,text);
	pattern = copy(text,14,19);

	cout << "\nTEXT: " << text << endl;
	cout << "PATTERN: " << pattern << endl;
	start = bruteforce(text,pattern);
	cout << "The pattern starts at " << start << endl;


	cout << "\n------------------------------------------\n" << endl;


	pattern = copy(text,0,3);

	cout << "\nTEXT: " << text << endl;
	cout << "PATTERN: " << pattern << endl;
	start = bruteforce(text,pattern);
	cout << "The pattern starts at " << start << endl;


	cout << "\n------------------------------------------\n" << endl;


	text = "aaaaaaaaaaaaaaaaaaab";
	pattern = "aaaab";

	cout << "\nTEXT: " << text << endl;
	cout << "PATTERN: " << pattern << endl;
	start = bruteforce(text,pattern);
	cout << "The pattern starts at " << start << endl;


	cout << "\n------------------------------------------\n" << endl;


	text = "aaaaaaaaaaaaaaaaaaab";
	int l = text.length();
	pattern = copy(text,l-3,l);

	cout << "\nTEXT: " << text << endl;
	cout << "PATTERN: " << pattern << endl;
	start = bruteforce(text,pattern);
	cout << "The pattern starts at " << start << endl;

	return 0;
}

int bruteforce(string word, string pattern) {
	int n = word.length();
	int m = pattern.length();
	int c = 0;

	for (int i=0; i<=(n-m); i++) {
		int j=0;
		while (j<m && (pattern[j] == word[i+j])) {
			j++;
		}
		c += j;
		if (j==m) {
			cout << "The \"j<m\" comparison operation was executed " << c << " times" << endl;
			return i;
		}
	}
	cout << "The \"j<m\" comparison operation was executed " << c << " times" << endl;
	return -1;
}

string copy(string s, int a, int b) {
	string c;
	for (int i=a;i<b;i++)
		c += s[i];
	return c;
}
