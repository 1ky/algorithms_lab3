#include <iostream>
#include <vector>
#include <ctime>
#include "pledge.h"

using namespace std;

vector<int> mergesort(vector<int> v);
vector<int> merge(vector<int> v1, vector<int> v2, vector<int> v);

int main() {
	pledge();

	vector<int> v;
	for (int i=0;i<16;i++)
		v.push_back(rand()%150);

	cout << "A list of 16 pseudo-random integers" << endl;
	for (int i=0;i<v.size();i++)
		cout << v[i] << " ";
	cout << endl;

	v = mergesort(v);
	cout << "\nAfter applying the merge sort algorithm the list is now in this order" << endl;
	for (int i=0;i<v.size();i++)
		cout << v[i] << " ";
	cout << endl;

	return 0;
}

vector<int> mergesort(vector<int> v) {
	vector<int> v1;
	vector<int> v2;

	int n = v.size();
	if (n>1) {
		for (int i=0;i<(n/2);i++)
			v1.push_back(v[i]);

		for (int i=(n/2);i<n;i++)
			v2.push_back(v[i]);

		v1 = mergesort(v1);
		v2 = mergesort(v2);

		return merge(v1,v2,v);
	} else {
		return v;
	}
}

vector<int> merge(vector<int> v1, vector<int> v2, vector<int> v) {
	int p = v1.size();
	int q = v2.size();
	int i = 0;
	int j = 0;
	int k = 0;

	while (i<p && j<q) {
		if (v1[i] <= v2[j])
			v[k] = v1[i++];
		else
			v[k] = v2[j++];
		k++;
	}

	if (i == p) {
		for (j;j<q;j++,k++)
			v[k] = v2[j];
	} else {
		for (i;i<p;i++,k++)
			v[k] = v1[i];
	}
	return v;
}
